<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 23/04/18
 * Time: 14:10
 */

namespace src\models;

class Lista
{
    /**
     * @var \PDO;
     */
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getLista(): array
    {
        $sql = $this->db->query("SELECT * FROM lista");
        if ($sql->rowCount() > 0) {
            return $sql->fetchAll();
        }

        return [];
    }

    public function add($data): void
    {
        $stmt = $this->db->prepare("INSERT INTO lista (nome, telefone) VALUES (:nome, :telefone)");
        $stmt->bindValue(':nome', $data['nome'], \PDO::PARAM_STR);
        $stmt->bindValue(':telefone', $data['telefone'], \PDO::PARAM_INT);
        $stmt->execute();
    }

}