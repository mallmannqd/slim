<?php

use Slim\Http\Request;
use Slim\Http\Response;
use \src\models\Lista;

// Routes

$app->get('/', function (Request $request, Response $response, array $args){
    $lista = new Lista($this->db);

    $args['lista'] = $lista->getLista();

    return $this->renderer->render($response, 'home.phtml', $args);
});

$app->get('/add', function (Request $request, Response $response, array $args){
    return $this->renderer->render($response, 'add.phtml', $args);
});

$app->post('/add', function (Request $request, Response $response, array $args){
    $data = $request->getParsedBody();

    $lista = new Lista($this->db);
    $lista->add($data);

    return $response->withStatus(302)->withHeader('Location', '/');
});